package com.xhlab.engram

import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.Logger

@Mod(modid = EngramMod.MODID, name = EngramMod.NAME, version = EngramMod.VERSION)
class EngramMod {

    companion object {
        const val MODID = "engrammod"
        const val NAME = "Engram Mod"
        const val VERSION = "0.1"

        private lateinit var logger: Logger
    }

    @Mod.EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        logger = event.modLog
    }

    @Mod.EventHandler
    fun init(event: FMLInitializationEvent) {

    }
}